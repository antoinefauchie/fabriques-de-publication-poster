# Les fabriques de publication : poster

Poster réalisé en juin 2020 avec [ReLaXed](https://github.com/RelaxedJS/ReLaXed) dans le cadre de l'École d’été du CERIUM "Humanités numériques : Approche interdisciplinaire" organisé par Michael Sinatra à l'Université de Montréal.
